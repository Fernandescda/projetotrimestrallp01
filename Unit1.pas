unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Mask;

type
  TForm1 = class(TForm)
    EditNome: TEdit;
    MaskEditDataNasc: TMaskEdit;
    EditPremio: TEdit;
    ListBoxAtores: TListBox;
    ButtonEnviar: TButton;
    ButtonApagar: TButton;
    ButtonAtualizar: TButton;
    procedure EditNomeClick(Sender: TObject);
    procedure EditPremioClick(Sender: TObject);
    procedure ButtonEnviarClick(Sender: TObject);
    procedure ButtonApagarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  Tatores = class(TObject)
    nome: string;
    datanasc: string;
    premios: string;
    end;

var
  Form1: TForm1;
  atores : Tatores;

implementation

{$R *.dfm}

procedure TForm1.EditNomeClick(Sender: TObject);
begin
      EditNome.clear;
end;

procedure TForm1.EditPremioClick(Sender: TObject);
begin
      EditPremio.clear;
end;

procedure TForm1.ButtonEnviarClick(Sender: TObject);
begin
     ListBoxAtores.Items.add(EditNome.Text);
     ListBoxAtores.Items.add(MaskEditDataNasc.Text);
     ListBoxAtores.Items.add(EditPremio.Text);
end;

procedure TForm1.ButtonApagarClick(Sender: TObject);
begin
     ListBoxAtores.DeleteSelected;
end;

end.
