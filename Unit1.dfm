object Form1: TForm1
  Left = 192
  Top = 125
  Width = 928
  Height = 480
  Caption = 'Form1'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object EditNome: TEdit
    Left = 8
    Top = 8
    Width = 161
    Height = 21
    TabOrder = 0
    Text = 'Nome'
    OnClick = EditNomeClick
  end
  object MaskEditDataNasc: TMaskEdit
    Left = 8
    Top = 40
    Width = 161
    Height = 21
    EditMask = '!99/99/00;1;_'
    MaxLength = 8
    TabOrder = 1
    Text = '  /  /  '
  end
  object EditPremio: TEdit
    Left = 8
    Top = 72
    Width = 161
    Height = 21
    TabOrder = 2
    Text = 'Premios Ganhos'
    OnClick = EditPremioClick
  end
  object ListBoxAtores: TListBox
    Left = 176
    Top = 8
    Width = 161
    Height = 185
    ItemHeight = 13
    TabOrder = 3
  end
  object ButtonEnviar: TButton
    Left = 8
    Top = 104
    Width = 161
    Height = 25
    Caption = 'Enviar'
    TabOrder = 4
    OnClick = ButtonEnviarClick
  end
  object ButtonApagar: TButton
    Left = 8
    Top = 136
    Width = 161
    Height = 25
    Caption = 'Apagar'
    TabOrder = 5
    OnClick = ButtonApagarClick
  end
  object ButtonAtualizar: TButton
    Left = 8
    Top = 168
    Width = 161
    Height = 25
    Caption = 'Atualizar'
    TabOrder = 6
  end
end
